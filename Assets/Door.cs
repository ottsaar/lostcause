﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {


    public GameObject target;
    private bool moving;
    private float timer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (target == null)
        {
            moving = true;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -2f);
        }

        if (moving)
        {
            timer += Time.deltaTime;
            if (timer > 15f)
                Destroy(gameObject);
        }
		
	}
}
