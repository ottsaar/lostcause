﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTrap : MonoBehaviour {

    public float damage = 100;
    private bool active = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < -2f)
        {
            GameObject.Destroy(transform.parent.gameObject);
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        PlayerController s = col.gameObject.GetComponent<PlayerController>();
        if (s && GetComponent<Rigidbody2D>().velocity.sqrMagnitude > 0.1 && active)
        {
            print(GetComponent<Rigidbody2D>().velocity.sqrMagnitude);
            s.TakeDamage(damage, col.gameObject.transform.position - transform.position);
            active = false;
        }
    }

}
