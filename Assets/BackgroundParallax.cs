﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour {

    // Use this for initialization
    public CameraMovement camera;
    private Vector3 lastPos;
    private int counter = 0;

	void Start () {
	}
	

	// Update is called once per frame
	void LateUpdate () {


        Vector3 delta = lastPos - camera.transform.position;
        transform.position = new Vector2(transform.position.x + delta.x * -0.3f, transform.position.y + delta.y * -0.1f);
        lastPos = camera.transform.position;
	}
}
