﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {


    public Rigidbody2D fallingObject;
    // Use this for initialization
    private bool falling;

    private bool active = true;

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if(falling && fallingObject.simulated == false && active)
        {
            fallingObject.simulated = true;
        }

                
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.GetComponent<PlayerController>() && active)
        {
            falling = true;
        } 
    }
    
}
