﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float cameraSpeed;
    public GameObject followObject; 
	// Use this for initialization

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (followObject == null)
            return;

        float vertical = Camera.main.orthographicSize;
        float horizontal = Camera.main.orthographicSize * Screen.width / Screen.height;
        float posDif;

        if (Mathf.Abs(transform.position.x - followObject.transform.position.x) > horizontal * 0.2 || Mathf.Abs(transform.position.y - followObject.transform.position.y) > vertical * 0.2)
        {
            posDif = (followObject.transform.position - transform.position).magnitude / 3;
            transform.position = Vector3.Lerp(transform.position, followObject.transform.position,posDif*posDif* Time.deltaTime * cameraSpeed);
            transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
        }
        //This makes it so the camera wouldn't be rotated on x, so no lines between tiles..
    }
}
