﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandWeapon : Weapon {
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public override void PerformAttack(float speed)
    {
        anim.speed = speed;
        anim.SetTrigger("Attack");
    }
}
