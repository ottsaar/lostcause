﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : Weapon
{
    //Range weapon is more of a projectile starting point, no need for animations and so on..
    public float projectileSpeed;
    public GameObject projectile;
    public override void PerformAttack(float speed)
    {
        GameObject obj = Instantiate(projectile, transform.position, Quaternion.identity);
        ProjectileWeapon temp = obj.GetComponent<ProjectileWeapon>();
        temp.damage = damage;
        temp.speed = projectileSpeed;
        Vector3 pos = GetComponentInParent<EnemyController>().target.position;
        temp.target = pos;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
       //DO NOTHING!!!
    }
}
