﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : Wearable {
	public float damage;
    public float player_bonus=0;

    public abstract void PerformAttack(float speed);
    public virtual void boxCollider(bool state)
    {
        if(transform.GetComponent<BoxCollider2D>() != null)
            transform.GetComponent<BoxCollider2D>().enabled = state;
    }
    public virtual Animator GetAnimator()
    {
        return transform.GetComponent<Animator>();
    }

	public virtual void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Enemy" && transform.parent.tag != "Enemy") {
            col.GetComponent<EnemyController>().TakeDamage(damage, transform.parent.localScale);
            boxCollider(false);
        }
        else if (transform.parent != null) { 
		    if (col.tag == "Player" && transform.parent.tag != "Player") {
                if (col.GetComponent<PlayerController>())
                    col.GetComponent<PlayerController>().TakeDamage(damage + bonus_Damage + player_bonus, transform.parent.localScale);
                else
                    col.transform.parent.GetComponent<PlayerController>().TakeDamage(damage, transform.parent.localScale);
                boxCollider(false);
            }
        }
	}

}
