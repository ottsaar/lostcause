﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeapon : Weapon {
    public float speed;
    public Vector3 target;
    private float timeCounter;
    void Start()
    {
        timeCounter = Time.time;
        target = new Vector2(target.x,target.y+1);
        Vector2 pos = target;
        Vector2 direction = (pos - (Vector2)transform.position).normalized;
        transform.up = direction;
    }

    void Update()
    {
        if(timeCounter + 5 < Time.time)
        {
            Destroy(gameObject);
        }
        transform.position += transform.up * speed * Time.deltaTime;
    }

    public override void PerformAttack(float speed)
    {
        //no need to do anything..
    }
   public override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy" )
        {

        }
        else if (col.tag == "Player")
        {
            if (col.GetComponent<PlayerController>())
                col.GetComponent<PlayerController>().TakeDamage(damage,transform.forward);
            else
                col.transform.parent.GetComponent<PlayerController>().TakeDamage(damage,transform.forward);
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
