﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wearable : MonoBehaviour{
    public float bonus_Speed=0;
    public float bonus_AttackSpeed = 0;
    public float bonus_Damage = 0;
    public float bonus_Health = 0;
    public float bonus_Mana = 0;
    public float bonus_Defense = 0;
}
