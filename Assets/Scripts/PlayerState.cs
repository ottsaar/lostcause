﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerState {
    public float health;
    public float mana;
    public int skillpoints;
    public float x;
    public float y;
    public int level;
    public bool[] skillActivations;

    public int skill_1;
    public int skill_2;
    public int skill_3;
}

