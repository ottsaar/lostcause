﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public string sceneName;
    public string nextLevelName;

    public GameObject player;
    GameObject boss;
    bool first = true;
    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
        boss = GameObject.Find("Boss");


        if (sceneName != "TutorialLevel")
        {
            LoadState(true);
        }

        if (IsLoading())
        {
            LoadState(false);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (Input.GetKeyDown(KeyCode.J))
        {
            PlayerController pc = player.GetComponent<PlayerController>();
            PlayerState state = GetState(pc);
            SaveGame(state);
        }

        if (Input.GetKeyDown(KeyCode.L))
            LoadGame();

        if (isPlayerDead() || isPlayerOffTheMap())
            SceneManager.LoadScene(sceneName);
        else if (isBossDead())
        {
            PlayerController pc = player.GetComponent<PlayerController>();
            PlayerState state = GetState(pc);
            state.x = -101f;
            state.y = -1f;
            state.level += 1;
            state.health = -1;
            state.mana = -1;
            SaveGame(state);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    private bool isPlayerDead()
    {
        if (player.GetComponent<PlayerController>().currentHealth <= 0f)
            return true;
        else
            return false;
    }

    private bool isBossDead()
    {
        if (boss == null)
            return true;
        if (boss.GetComponent<EnemyController>().currentHealth <= 0f)
            return true;
        else
            return false;
    }

    private bool isPlayerOffTheMap()
    {
        if (player.transform.position.y < 0f)
            return true;
        else
            return false;

    }


    private void SaveGame(PlayerState state)
    {

        string jsonString = JsonUtility.ToJson(state);

        using (StreamWriter streamWriter = File.CreateText(Path.Combine(Application.persistentDataPath, "save.txt")))
        {
            streamWriter.Write(jsonString);
        }

    }

    private void LoadGame()
    {
        PlayerController pc = player.GetComponent<PlayerController>();
        using (StreamReader streamReader = File.OpenText(Path.Combine(Application.persistentDataPath, "save.txt")))
        {
            SetLoading(1);
            string jsonString = streamReader.ReadToEnd();
            PlayerState state = JsonUtility.FromJson<PlayerState>(jsonString);
            SceneManager.LoadScene(state.level);
        }
    }

    private void LoadState(bool newlevel)
    {
        using (StreamReader streamReader = File.OpenText(Path.Combine(Application.persistentDataPath, "save.txt")))
        {
            PlayerController pc = player.GetComponent<PlayerController>();
            string jsonString = streamReader.ReadToEnd();
            PlayerState state = JsonUtility.FromJson<PlayerState>(jsonString);

            pc.currentHealth = state.health >= 0 ? state.health : pc.maxHealth;
            pc.currentMana = state.mana >= 0 ? state.mana : pc.maxMana;
            pc.TakeDamage(0f, Vector2.zero);
            pc.DecreaseMana(0f);

            if (state.x > -100)
                pc.transform.position = new Vector2(state.x, state.y);

            pc.GetComponent<SkillManager>().skillPointsAmmount = state.skillpoints;
            SetLoading(0);
        }

        LoadSkillTreeState();
    }

    private void LoadSkillTreeState()
    {
        using (StreamReader streamReader = File.OpenText(Path.Combine(Application.persistentDataPath, "save.txt")))
        {
            PlayerController pc = player.GetComponent<PlayerController>();
            string jsonString = streamReader.ReadToEnd();
            PlayerState state = JsonUtility.FromJson<PlayerState>(jsonString);
            SkillManager s = pc.GetComponent<SkillManager>();
            bool[] skillactivations = state.skillActivations;
            List<bool> activations = new List<bool>(skillactivations);
            SetSkillTreeState(s.GetSkillTree(), ref activations);
            GameObject.Find("SkillTreeCanvas").GetComponent<SkillTreeController>().UpdateUI(s.GetSkillTree());

            if(state.skill_1 != null)
                s.SelectSkill(0, FindSkillTreeNode(s.GetSkillTree(), state.skill_1));
            if (state.skill_2 != null)
                s.SelectSkill(1, FindSkillTreeNode(s.GetSkillTree(), state.skill_2));
            if (state.skill_3 != null)
                s.SelectSkill(2, FindSkillTreeNode(s.GetSkillTree(), state.skill_3));

            s.upgrades.Update(s.GetSkillTree());

        }
    }

    private PlayerState GetState(PlayerController pc)
    {
        PlayerState state = new PlayerState();
        state.health = pc.currentHealth;
        state.mana = pc.currentMana;
        state.x = pc.transform.position.x;
        state.y = pc.transform.position.y;


        SkillManager skillmanger = pc.GetComponent<SkillManager>();
        state.skillpoints = skillmanger.skillPointsAmmount;

        List<bool> skillActivations = new List<bool>();
        SkillTreeState(skillmanger.GetSkillTree(), ref skillActivations);
        bool[] activations = skillActivations.ToArray();
        state.skillActivations = activations;

        print(activations.Length);

        state.level = SceneManager.GetActiveScene().buildIndex;

        if(skillmanger.GetSelectedSkill(0) != null)
            state.skill_1 = skillmanger.GetSelectedSkill(0).index;
        if (skillmanger.GetSelectedSkill(1) != null)
            state.skill_2 = skillmanger.GetSelectedSkill(1).index;
        if (skillmanger.GetSelectedSkill(2) != null)
            state.skill_3 = skillmanger.GetSelectedSkill(2).index;

        return state;
    }


    private void SetLoading(int loading)
    {
        PlayerPrefs.SetInt("loading", loading);
        PlayerPrefs.Save();
    }

    public void SkillTreeState(SkillTreeNode tree, ref List<bool> activations)
    {
        foreach (SkillTreeNode child in tree.children)
        {
            activations.Add(child.IsActivated());
            SkillTreeState(child, ref activations);
        }
    }


    public SkillTreeNode FindSkillTreeNode(SkillTreeNode tree, int index)
    {
        foreach (SkillTreeNode child in tree.children)
        {
            if (index == child.index)
                return child;
            else {
                SkillTreeNode other = FindSkillTreeNode(child, index);
                if (other != null)
                    return other;
             }
        }

        return null;
    }

    public void LoadNewLevelState()
    {

    }

    public void SetSkillTreeState(SkillTreeNode tree, ref List<bool> activations)
    {
        foreach (SkillTreeNode child in tree.children)
        {
            if (activations[0] == true)
                child.Activate();

            activations.RemoveAt(0);
            SetSkillTreeState(child, ref activations);
        }
    }

    private bool IsLoading()
    {
        return PlayerPrefs.GetInt("loading") == 1;
    }

}
