﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMusicTrigger : MonoBehaviour {
    public AudioClip musicToChangeTo;
    void OnTriggerEnter2D(Collider2D col)
    {
        Camera.main.transform.GetComponent<AudioSource>().clip = musicToChangeTo;
        Camera.main.transform.GetComponent<AudioSource>().Play();
        Destroy(this);
    }
}
