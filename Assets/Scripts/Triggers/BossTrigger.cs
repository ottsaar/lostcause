﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossTrigger : MonoBehaviour {
    public GameObject bossBar;
    public Image BossHealth;
    private EnemyController boss;
	void Start()
    {
        boss = GameObject.Find("Boss").GetComponent<EnemyController>();
    }
    void Update()
    {
        if (bossBar.activeInHierarchy)
        {
            BossHealth.fillAmount = boss.currentHealth / boss.maxHealth;
        }
    }
	void OnTriggerEnter2D(Collider2D col)
    {
        bossBar.SetActive(true);
    }
}
