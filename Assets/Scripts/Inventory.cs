﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
	//Weapon ref needed to be chaned
	public GameObject weapon;
    //UI elements
    public Image weaponUI;
    public Image helmUI;
    public Image armorUI;
    public Image pantsUI;
    public Image bootsUI;
    public Image WeaponIcon;
    public GameObject inventoryUI;

    private PlayerController player;
    private bool wepEquipped = false;

    public void Start()
    {
        player = transform.GetComponent<PlayerController>();
    }
    //Equips item
    public void Equip(GameObject obj){
        //Weapon needs special treatment, because made attack system, just updates values
		if(obj.GetComponent<Weapon>()!=null){
            equipHelper(obj,weaponUI);
            //Enables attack animations and does a rootMotion fix
            weapon.GetComponent<Animator>().enabled = true;
            weapon.GetComponent<Animator>().applyRootMotion = true;
            //Changing attributes of the player controlled weapon
            weapon.GetComponent<SpriteRenderer>().sprite = obj.GetComponent<SpriteRenderer>().sprite; 
            weapon.GetComponent<HandWeapon>().damage = obj.GetComponent<HandWeapon>().damage;
            weapon.GetComponent<HandWeapon>().bonus_AttackSpeed = obj.GetComponent<HandWeapon>().bonus_AttackSpeed;
            weapon.GetComponent<HandWeapon>().bonus_Damage = obj.GetComponent<HandWeapon>().bonus_Damage;
            weapon.GetComponent<HandWeapon>().bonus_Defense = obj.GetComponent<HandWeapon>().bonus_Defense;
            weapon.GetComponent<HandWeapon>().bonus_Health = obj.GetComponent<HandWeapon>().bonus_Health;
            weapon.GetComponent<HandWeapon>().bonus_Mana = obj.GetComponent<HandWeapon>().bonus_Mana;
            weapon.GetComponent<HandWeapon>().bonus_Speed = obj.GetComponent<HandWeapon>().bonus_Speed;
            //Ui icon.
            WeaponIcon.enabled = true;
            WeaponIcon.sprite = obj.GetComponent<SpriteRenderer>().sprite;
            wepEquipped = true;
            weapon.GetComponent<Animator>().applyRootMotion = false;
        }
        else if (obj.GetComponent<Armor>() != null)
        {
            equipHelper(obj,armorUI);
        }
        else if (obj.GetComponent<Pants>() != null)
        {
            equipHelper(obj,pantsUI);
        }
        else if (obj.GetComponent<Boots>() != null)
        {
            equipHelper(obj,bootsUI);
        }
        else if (obj.GetComponent<Helm>() != null)
        {
            equipHelper(obj,helmUI);
        }
    }
    private void equipHelper(GameObject addable,Image uiElem) 
    {
        RemoveItem(addable);
        if (uiElem.transform.parent.GetComponent<Wearable>() != null)
        {
            AddItem(uiElem.transform.parent.GetComponent<Wearable>().gameObject);
        }
        uiElem.sprite = addable.GetComponent<SpriteRenderer>().sprite;
        uiElem.preserveAspect = true;
        addable.transform.parent = uiElem.transform.parent;
        uiElem.gameObject.SetActive(true);
        calculateBonuses(addable.GetComponent<Wearable>(), true);
    }
    //Deequips item
    public void Deequip(GameObject obj)
    {
        if (obj.GetComponent<Weapon>() != null)
        {
            deequipHelper(obj,weaponUI);
            //makes everything zero for wep
            weapon.GetComponent<Animator>().enabled = false;
            weapon.GetComponent<SpriteRenderer>().sprite = null;
            weapon.GetComponent<HandWeapon>().damage = 0;
            weapon.GetComponent<HandWeapon>().bonus_AttackSpeed = 0;
            weapon.GetComponent<HandWeapon>().bonus_Damage = 0;
            weapon.GetComponent<HandWeapon>().bonus_Defense = 0;
            weapon.GetComponent<HandWeapon>().bonus_Health = 0;
            weapon.GetComponent<HandWeapon>().bonus_Mana = 0;
            weapon.GetComponent<HandWeapon>().bonus_Speed = 0;
            //Ui icon.
            WeaponIcon.enabled = false;
            WeaponIcon.sprite = null;
            wepEquipped = false;
        }
        else if (obj.GetComponent<Armor>() != null)
        {
            deequipHelper(obj,armorUI);
        }
        else if (obj.GetComponent<Pants>() != null)
        {
            deequipHelper(obj,pantsUI);
        }
        else if (obj.GetComponent<Boots>() != null)
        {
            deequipHelper(obj,bootsUI);
        }
        else if (obj.GetComponent<Helm>() != null)
        {
            deequipHelper(obj,helmUI);
        }
    }
    private void deequipHelper(GameObject addable,Image uiElem)
    {
        uiElem.gameObject.SetActive(false);
        AddItem(addable);
        calculateBonuses(addable.GetComponent<Wearable>(), false);
    }
    //Calculates bonuses given to player
    public void calculateBonuses(Wearable item,bool add)
    {
        if (add)
        {
            player.attackSpeed += item.bonus_AttackSpeed;
            player.damage += item.bonus_Damage;
            player.defense += item.bonus_Defense;
            player.maxHealth += item.bonus_Health;
            player.maxMana += item.bonus_Mana;
           
            player.runMovementSpeed += item.bonus_Speed;
        }
        else
        {
            player.attackSpeed -= item.bonus_AttackSpeed;
            player.damage -= item.bonus_Damage;
            player.defense -= item.bonus_Defense;
            player.maxHealth -= item.bonus_Health;
            player.maxMana -= item.bonus_Mana;
       
            player.runMovementSpeed -= item.bonus_Speed;
        }
    }
    //add an item into the inventory
    public void AddItem(GameObject item)
    {
        int childCount = inventoryUI.transform.childCount;

        for(int i = 0;i<childCount;i++)
        {
            Image slot = inventoryUI.transform.GetChild(i).GetComponent<Image>();
            if (slot.GetComponentInChildren<Wearable>() == null)
            {
                item.transform.parent = slot.transform;
                item.transform.position = Vector2.zero;
                slot.transform.GetChild(0).gameObject.SetActive(true);
                slot.GetComponentsInChildren<Image>()[1].sprite = item.GetComponent<SpriteRenderer>().sprite;
                slot.GetComponentsInChildren<Image>()[1].preserveAspect = true;
                break;
            }
            else if(slot.GetComponentInChildren<Wearable>().gameObject == item)
            {
                break;
            }
        }
    }
    //drops the item on the ground and removes from inventory
    public void DropItem(GameObject item)
    {
        int childCount = inventoryUI.transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            Image slot = inventoryUI.transform.GetChild(i).GetComponent<Image>();
            if (slot.GetComponentInChildren<Wearable>() == item.GetComponent<Wearable>())
            {
                if (item.GetComponent<Animator>())
                    item.GetComponent<Animator>().enabled = false;
    
                item.transform.parent = null;
                item.transform.position = transform.position+new Vector3(0,0.5f,0);
                item.transform.localPosition = transform.position + new Vector3(0, 0.5f, 0);
                slot.GetComponentsInChildren<Image>()[1].sprite = null;
                slot.GetComponentsInChildren<Image>()[1].gameObject.SetActive(false);
                break;
            }
        }
    }
    //free's the inventory spot
    public void RemoveItem(GameObject item)
    {
        int childCount = inventoryUI.transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            Image slot = inventoryUI.transform.GetChild(i).GetComponent<Image>();
            if (slot.GetComponentInChildren<Wearable>() == item.GetComponent<Wearable>())
            {
                item.transform.parent = null;
                slot.GetComponentsInChildren<Image>()[1].sprite = null;
                slot.GetComponentsInChildren<Image>()[1].gameObject.SetActive(false);
                break;
            }
        }
    }
	public Weapon GetWeapon(){
        if (!wepEquipped)
            return null;
		return weapon.GetComponent<Weapon>();
	}
}
