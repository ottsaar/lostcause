﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockWave : MonoBehaviour {
	public int waveDamage = 20;
	public float speedOfWave = 10;
	private float wavePower;
	private int dir;
	private float timer;

	// Use this for initialization
	void Start () {
		timer = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += new Vector3(dir*speedOfWave*Time.deltaTime,0,0);
		if(Time.time > timer+wavePower)
			Destroy(gameObject);
	}
	public void SetPower(float power,int dir){
		this.dir = dir;
		wavePower = power/2;
	}
	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Player"){
			if(col.GetComponent<PlayerController>()!=null)
				col.GetComponent<PlayerController>().TakeDamage(waveDamage,transform.forward);
			else
				col.transform.parent.GetComponent<PlayerController>().TakeDamage(waveDamage,transform.forward);
		}
	}
}
