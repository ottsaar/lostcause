﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeScript : MonoBehaviour
{

    public RopeEndPointScript endPointOfTheRope;
    public Vector2 target;
    public List<Rigidbody2D> joints;
    public float maxLength;
    public float speed;

    public Rigidbody2D owner;

    public Rigidbody2D ropeSegmentPrefab;


    private float curLength;
    public int state;
    private Vector2 direction;

    private float duration = 0f;

    public bool done;



    // 0 - moving the endpoint to target
    // 1 - making joints
    // 2 - hanging


    void Start()
    {
        duration = 0f;
    }

    void Update()
    {
        UpdateLines();

        if (duration > 2)
        {
            done = true;
            return;
        }

        if (state == 1)
        {
            joints[0].GetComponent<HingeJoint2D>().autoConfigureConnectedAnchor = false;
            state = 2;
        }
        if (state == 2)
        {
            duration += Time.deltaTime;
            return;
        }

        if (endPointOfTheRope.done && endPointOfTheRope.gotTarget)
        {
            curLength = endPointOfTheRope.length;
            CreateSegments();
        }
        else if (endPointOfTheRope.done)
        {
            done = true;
        }
    }
    private void UpdateLines()
    {
        LineRenderer lines = GetComponent<LineRenderer>();
        if (state == 2)
        {
            Vector3[] positions = new Vector3[joints.Count + 1];
            lines.positionCount = joints.Count + 1;

            int i = 0;
            foreach (Rigidbody2D joint in joints)
            {
                positions[i] = joint.transform.position;
                i++;
            }
            positions[i] = endPointOfTheRope.transform.position;
            lines.SetPositions(positions);  
        }
        else if(state == 0)
        {
            Vector3[] positions = new Vector3[2];
            lines.positionCount = 2;
            positions[0] = owner.transform.position;
            positions[1] = endPointOfTheRope.transform.position;
            lines.SetPositions(positions);
        }
    }

    private void CreateSegments()
    {
        Vector2 s = owner.transform.position;

        direction = (endPointOfTheRope.transform.position - owner.transform.position);
        curLength = direction.magnitude;
        direction.Normalize();

        int segments = Mathf.RoundToInt(curLength*2);
        float sLength = curLength / segments;

        Rigidbody2D lastSeg = owner;
        for (int i = 1; i < segments; i++)
        {
            lastSeg = CreateSegment(new Vector2(owner.transform.position.x, owner.transform.position.y) + i * sLength * direction, lastSeg);
        }

        endPointOfTheRope.GetComponent<HingeJoint2D>().connectedBody = lastSeg;

        state = 1;
    }

    private Rigidbody2D CreateSegment(Vector2 position, Rigidbody2D lastSegment)
    {
        Rigidbody2D segm = (Rigidbody2D)Instantiate<Rigidbody2D>(ropeSegmentPrefab);
        segm.GetComponent<HingeJoint2D>().connectedBody = lastSegment;
        segm.transform.SetParent(transform);
        segm.transform.position = position;
        joints.Add(segm);
        return segm;
    }

    public void InitValues(float maxLength, Vector2 target, float speed, Rigidbody2D owner)
    {
        this.maxLength = maxLength;
        this.target = target;
        this.speed = speed;
        this.owner = owner;
        state = 0;
        done = false;
        direction = (target - new Vector2(transform.position.x, transform.position.y)).normalized;
        endPointOfTheRope.StartMoving(direction, speed, maxLength, transform.position);
    }



}
