﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class feetColliderScript : MonoBehaviour {

	private PlayerController playerController;
	// Use this for initialization
	void Start () {
		playerController = GetComponentInParent<PlayerController>();
	}
	
	void OnCollisionEnter2D(Collision2D col){
		if(col.transform.tag == "Enemy"){
			//Damaging probably
		}
		playerController.jumpCount  = 2;
        playerController.isGrounded(true);
    }
	void OnCollisionExit2D(Collision2D col){
		playerController.jumpCount = 1;
        playerController.isGrounded(false);
	}
}
