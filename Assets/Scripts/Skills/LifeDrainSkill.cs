﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeDrainSkill : Skill {

    bool done;
    Vector2 targetPos;
    float drainDuration = 10;
    GameObject targetObject;
    float damagePerSecond = 1f;
    GameObject particles;



    public LifeDrainSkill(GameObject gameObject, Vector2 tP, ActiveSkillData skill) : base(gameObject, skill)
    {
        done = false;
        targetPos = tP;
        cooldown = 4;

        if (TargetUnderMouse(targetPos) == null)
            done = true;
        else
        {
            targetObject = TargetUnderMouse(targetPos).gameObject;
            if (targetObject.GetComponent<EnemyController>() == null)
                done = true;
        }
        if (!done)
        {
            particles = Object.Instantiate(Resources.Load("LifeDrainParticles") as GameObject);
            particles.transform.parent = targetObject.transform;
            particles.transform.localPosition = new Vector3(0, 0, 0);
            particles.GetComponent<LifeDrainParticleScript>().target = gameObject;
        }
    }

    public override bool IsFinished()
    {
        return done;
    }

    public override float getCooldown()
    {
        return cooldown;
    }

    public override void Update()
    {
        if (done)
            return;

        base.Update();

        if (targetObject == null)
        {
            end();
            return;
        }

        if (duration < drainDuration)
        {
            targetObject.GetComponent<EnemyController>().TakeDamage(Time.deltaTime * damagePerSecond,new Vector2(0,0));
            gameObject.GetComponent<PlayerController>().AddHealth(Time.deltaTime * damagePerSecond);
        }
        else
            end();
    }

    void end()
    {
        done = true;
        GameObject.Destroy(particles);
    }
}
