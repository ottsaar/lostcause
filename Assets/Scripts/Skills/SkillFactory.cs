﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillFactory{
    public Skill MakeSkill(GameObject player, Vector2 target, ActiveSkillData skillObject)
    {
        switch (skillObject.skillType)
        {
            case SkillType.Dash:
                return new DashSkill(player, target, skillObject);
            case SkillType.LifeDrain:
                return new LifeDrainSkill(player, target, skillObject);
            case SkillType.LaunchBomb:
                return new LaunchBomb(player, target, skillObject);
            case SkillType.RopeThrow:
                return new RopeThrowSkill(player, target, skillObject);
            case SkillType.RopePull:
                return new RopePullSkill(player, target, skillObject);
            case SkillType.PullEverything:
                return new PullEverythingSkill(player, target, skillObject);
            default:
                return null;
        }
    }
}

public enum SkillType
{
       Dash,
       LifeDrain,
       LaunchBomb, 
       RopeThrow,
       RopePull,
       PullEverything,
       //just adds some attribute to player character
       SimplePassive,
       ScriptedPassive
}


public enum SimplePassiveType
{
    DamageIncrease,
    DamageAmplification,
    SkillDamageAmplification,
    IncomingDamageReduction,
    SpeedIncrease,
    JumpSpeedIncrease,
    HealthIncrease,
    HealthRegeneration,
}

