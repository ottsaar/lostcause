﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashSkill : Skill
{ 
    //param1 impulse ammount
    //param2 == 0, then direction is fixed (lookDirection, 0, 0)
    //param2 == 1 , then direction is mouse position

    bool done;
    bool dashFinished;
    float velResetTime = 0.4f;
    Vector2 targetPos;
    GameObject particles;

    public DashSkill(GameObject gameObject, Vector2 tP, ActiveSkillData skill) : base(gameObject, skill)
    {

        done = false;
        targetPos = tP;
        cooldown = skill.cooldown;
        dashFinished = false;
        particles = Object.Instantiate(Resources.Load("DashParticles") as GameObject);
        particles.transform.parent = gameObject.transform;
        particles.transform.localPosition = new Vector3(0, 1f, 0);
        particles.transform.localScale = new Vector3(-1 * gameObject.transform.localScale.x, 1, 1);
    }

    public override bool IsFinished()
    {
        return dashFinished;  
    }

    public override float getCooldown()
    {
        return cooldown;
    }

	public override void Update () {
        base.Update();

        if (!done)
        {
            Rigidbody2D rigidBody = gameObject.GetComponent<Rigidbody2D>();
            rigidBody.velocity = Vector2.zero;
            if (skillData.param2 == 0)
                rigidBody.AddForce(new Vector2(gameObject.GetComponent<PlayerController>().lookDirection * skillData.param1, 0), ForceMode2D.Impulse);
            else if (skillData.param2 == 1) {
                Vector2 dir = targetPos - new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
                rigidBody.AddForce(dir.normalized * skillData.param1, ForceMode2D.Impulse);
            }
            done = true;
        }
        
        if(duration > velResetTime)
        {
            Rigidbody2D rigidBody = gameObject.GetComponent<Rigidbody2D>();
            rigidBody.velocity = Vector2.zero;
            dashFinished = true;
            GameObject.Destroy(particles);
        }
    }
}
