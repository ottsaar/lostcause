﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullEverythingSkill : Skill
{
    //param 1 dmg,
    //param 2 radius,
    //param 3 pull <0 or push if >0
    bool done;
    Vector2 targetPos;
    float damage;
    float radius;
    private bool hasTargets = false;

    List<EnemyController> targets;

    public PullEverythingSkill(GameObject gameObject, Vector2 tP, ActiveSkillData skill) : base(gameObject, skill)
    {
        radius = skill.param2;
        damage = skill.param1;

        cooldown = skill.cooldown;
        targets = new List<EnemyController>();
    }

    public override bool IsFinished()
    {
        return done;
    }

    public override float getCooldown()
    {
        return cooldown;
    }

    public override void Update()
    {
        if (done)
            return;

        if (!hasTargets)
            FindTargets();
        else
        {
            Vector2 dir;
            foreach (EnemyController t in targets)
            {
                if (skillData.param3 > 0)
                    dir = gameObject.transform.position - t.gameObject.transform.position;
                else
                    dir = t.gameObject.transform.position - gameObject.transform.position;

                dir.Normalize();
                t.gameObject.GetComponent<Rigidbody2D>().AddForce(dir * 15 + 2 * Vector2.up, ForceMode2D.Impulse);
            }
            done = true;
        }

        
    }

    private void FindTargets()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(gameObject.transform.position, radius);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject.GetComponent<EnemyController>() != null)
                targets.Add(colliders[i].gameObject.GetComponent<EnemyController>());
        }
        hasTargets = true;
    }
}
