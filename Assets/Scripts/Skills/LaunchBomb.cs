﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBomb : Skill
{

    bool done;
    Vector2 targetPos;
    GameObject projectile;
    float damage;

    public LaunchBomb(GameObject gameObject, Vector2 tP, ActiveSkillData skill) : base(gameObject, skill)
    {
        skill = (ActiveSkillData)skill;
        projectile = Object.Instantiate(Resources.Load("BombProjectile") as GameObject);
        Vector2 direction = tP - new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
        BombProjectile proBomb = projectile.GetComponent<BombProjectile>();

        proBomb.direction = direction;
        proBomb.speed = skill.param2;
        proBomb.maxDamage = skill.param1;
        proBomb.maxRange = skill.param2;

        direction.Normalize();
        projectile.gameObject.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 1) + direction * 2;

        cooldown = skill.cooldown;
    }

    public override bool IsFinished()
    {
        return done;
    }

    public override float getCooldown()
    {
        return cooldown;
    }

    public override void Update()
    {
        projectile.GetComponent<BombProjectile>().Launch();
        done = true;
    }

}
