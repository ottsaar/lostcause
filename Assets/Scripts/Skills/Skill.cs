﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill{

    public float cooldown = 2;
    public ActiveSkillData skillData;

    protected GameObject gameObject;
    protected float duration = 0;
    

    public Skill(GameObject gO, ActiveSkillData Skill)
    {
        gameObject = gO;
        skillData = Skill;
    }

    protected bool LineOfSight(Vector2 target)
    {
        RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, target- new Vector2(gameObject.transform.position.x, gameObject.transform.position.y));
        if (hit.collider == null)
            return false;
        else
            return true;
    }

    protected Collider2D TargetUnderMouse(Vector2 target)
    {
        RaycastHit2D hit = Physics2D.Raycast(target, Vector2.zero);
        return hit.collider;
    }

    public virtual void Update()
    {
        duration = duration + Time.deltaTime;
    }

    public virtual bool Activate()
    {
        return true;
    }

    abstract public float getCooldown();
    abstract public bool IsFinished();

    //Can receive extra input after skill is activated
    public virtual bool TakesExtraInput()
    {
        return false;
    }

    //Handles what happens if this skill is used again while its active
    public virtual void ExtraInput()
    {
        return;
    }


    

}
