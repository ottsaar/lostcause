﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeThrowSkill : Skill
{
    //param1 rope max length
    //param2 rope throw speed

    private Vector2 target;
    private bool done;
    private ActiveSkillData skill;

    private GameObject rope;

    public RopeThrowSkill(GameObject gameObject, Vector2 tP, ActiveSkillData skill) : base(gameObject, skill)
    {
        target = tP;
        done = false;
        this.skill = skill;

        rope = Object.Instantiate(Resources.Load("Rope") as GameObject);
        rope.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
        rope.GetComponent<RopeScript>().InitValues(skill.param1, target, skill.param2, gameObject.GetComponent<Rigidbody2D>());

    }

    public override float getCooldown()
    {
        return skill.cooldown;
    }

    public override bool IsFinished()
    {
        return done;
    }

    public override void Update()
    {
        if (done)
            return;

        base.Update();

        if (rope.GetComponent<RopeScript>().done)
        {
            GameObject.Destroy(rope);
            done = true;
        }
        
    }

    public override void ExtraInput()
    {
        done = true;
        GameObject.Destroy(rope);
        return;
    }

    public override bool TakesExtraInput()
    {
        return true;
    }

}
