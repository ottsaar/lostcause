﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	//Player related settings
	public float runMovementSpeed=5f;
	public float duckMovementSpeed=2f;
	public float maxHealth = 100;
	public float currentHealth = 100;
    public float attackSpeed = 1;
    public float damage = 0;
    public float defense = 0;
	private float movementSpeed;
	public float jumpingForce=2f;
	//Power related stuff
	public float dashPower = 2;
	public float shockWavePower = 1;
	public GameObject shockWave;

    public float maxMana = 100;
    public float currentMana = 100;
        
	public int jumpCount = 2;
	public int lookDirection = 1;//1 is right; -1 is left
    //UI STUFF
    public Image HealthBar;
    public Image ManaBar;
    public GameObject inventoryUI;

    private Rigidbody2D rigidbody;

	//private ParticleSystem dashParticle=null;
	private Animator anim;

	private BoxCollider2D boxCollider;
    private Camera cam;
    private GameObject aimTarget;
	private Inventory inventory;
    private SkillManager skillManager;
    private CharAudioManager audio;
    private bool grounded;
    private bool attacking;
    private float timeCounter;
    //unity can only bind axis values in input settings, so for keypresses these are necessary
    private KeyCode CastSkill1 = KeyCode.E;
    private KeyCode CastSkill2 = KeyCode.R;
    private KeyCode CastSkill3 = KeyCode.T;

    void Start () {
        anim = GetComponentsInChildren<Animator>()[0];
        cam = Camera.main;
		rigidbody = GetComponent<Rigidbody2D>();
		inventory = GetComponent<Inventory>();
		boxCollider = GetComponent<BoxCollider2D>();
		currentHealth = maxHealth;
        aimTarget = GameObject.Find("Player/AimTarget");
        skillManager = GetComponent<SkillManager>();
        audio = GetComponent<CharAudioManager>();
    }
	
	// Update is called once per frame
	void Update () {
        Attack();
        //SpecialAttack();
        HandleSkillsInput();
        if(!attacking)
		    Movement();
        HandleMouseMove();
        MouseClick();
        //Inventory opening
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (inventoryUI.activeInHierarchy)
                inventoryUI.SetActive(false);
            else
                inventoryUI.SetActive(true);
        }
	}
    public void MouseClick()
    {
        Vector2 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
            print(Vector2.Distance(transform.position, hit.point));
            if (hit.transform == null)
                return;
             if(hit.transform.GetComponent<Wearable>()!=null && Vector2.Distance(transform.position,hit.point) <= 2)
             {
                inventory.AddItem(hit.transform.gameObject);
             }
        }
        CheckUpgrades();
    }
	
    public void HandleMouseMove()
    {
        Vector2 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        aimTarget.transform.position = mousePos;
    }

    public void HandleSkillsInput()
    {
        //Somewhere here a ManaBar (similar to healtbar)
        Vector2 mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetKeyDown(CastSkill1))
            skillManager.Cast(0, mousePos);
        if (Input.GetKeyDown(CastSkill2))
            skillManager.Cast(1, mousePos);
        if (Input.GetKeyDown(CastSkill3))
            skillManager.Cast(2, mousePos);
    }
    void Attack(){
        //if no weapon equipped than can't attack.
        if (inventory.GetWeapon() == null)
            return;
        //main attack is over
        if (timeCounter + 0.4f/attackSpeed <= Time.time)
        {
            inventory.GetWeapon().boxCollider(false);
        }
        //little delay
        if (timeCounter + 0.65f / attackSpeed <= Time.time)
            attacking = false;
		if(Input.GetAxisRaw("AttackButton")!=0 && !attacking && grounded){
            anim.speed = 5*attackSpeed;
            inventory.GetWeapon().player_bonus = damage;
            inventory.GetWeapon().PerformAttack(attackSpeed);
            inventory.GetWeapon().boxCollider(true);
            audio.Attack();
            anim.SetTrigger("Attacking");
            anim.SetBool("Running", false);
            if (inventory.GetWeapon() != null)
                inventory.GetWeapon().GetAnimator().SetBool("Running", false);
            attacking = true;
            timeCounter = Time.time;
		}
	}
	//Character movement
	void Movement(){
		if(Input.GetAxis("Horizontal")!=0){
			if(!wallCollision(Input.GetAxis("Horizontal"))){
				//Makes the player look at the moving direction
				//dashParticle.transform.localScale = changeDir(-lookDirection,dashParticle.transform);
				transform.localScale = changeDir(lookDirection,transform);
				anim.SetBool("Running",true);
                //Weapon anim
                if (inventory.GetWeapon() != null)
                {
                    inventory.GetWeapon().GetAnimator().SetBool("Running", true);
                    inventory.GetWeapon().GetAnimator().speed = Mathf.Abs(Input.GetAxis("Horizontal"));
                }
                anim.speed=Mathf.Abs(Input.GetAxis("Horizontal")*5);
                //Adds movement
                rigidbody.velocity = new Vector2(0,rigidbody.velocity.y);
				transform.position += new Vector3(Input.GetAxis("Horizontal")*movementSpeed*Time.deltaTime,0);
                //if on the ground then make ground walking noise.
                if (grounded)
                {
                    audio.Walking();
                    audio.setMovPlayBackSpeed(Mathf.Abs(Input.GetAxis("Horizontal")));
                    if (anim.GetBool("Airborne"))
                    {
                        anim.SetBool("Airborne", false);
                        if (inventory.GetWeapon() != null)
                            inventory.GetWeapon().GetAnimator().SetBool("Airborne",false);
                        
                    } 
                }
                else
                {
                    audio.setMovPlayBackSpeed(1);
                    anim.SetBool("Airborne", true);
                    if (inventory.GetWeapon() != null)
                        inventory.GetWeapon().GetAnimator().SetBool("Airborne", true);
                }
            }
			else{
				anim.SetBool("Running",false);
                anim.speed = 5;
                if (inventory.GetWeapon() != null)
                {
                    inventory.GetWeapon().GetAnimator().SetBool("Running", false);
                    inventory.GetWeapon().GetAnimator().speed = 1;
                }
                audio.setMovPlayBackSpeed(1);
            }
		}
		else{
			anim.SetBool("Running",false);
            anim.speed = 5;
            if (inventory.GetWeapon() != null)
            {
                inventory.GetWeapon().GetAnimator().SetBool("Running", false);
                inventory.GetWeapon().GetAnimator().speed = 1;
            }
            audio.setMovPlayBackSpeed(1);
        }
		if(Input.GetKeyDown(KeyCode.W) && jumpCount > 0){
            audio.setMovPlayBackSpeed(1);
            audio.Jump();
            anim.SetTrigger("Jump");
            if (inventory.GetWeapon() != null)
            {
                inventory.GetWeapon().GetAnimator().SetTrigger("Jump");
                inventory.GetWeapon().GetAnimator().speed = 1;
            }
            print("jumping");
			rigidbody.velocity = new Vector2(rigidbody.velocity.x,0);
			rigidbody.AddForce(new Vector2(0,jumpingForce),ForceMode2D.Impulse);
			jumpCount--;			
		}
		if(Input.GetAxisRaw("Vertical")<0){
			print("Ducking here");
			boxCollider.offset = new Vector2(0,0.75f);
			boxCollider.size = new Vector2(0.745f,1);
			movementSpeed = duckMovementSpeed;
		}
		else{
			boxCollider.offset = new Vector2(0,1.16573f);
			boxCollider.size = new Vector2(0.745f,1.832159f);
			movementSpeed = runMovementSpeed;
		}
	}
    public void AddHealth(float ammount)
    {
        currentHealth += ammount;
        currentHealth = Mathf.Min(currentHealth, maxHealth);
        HealthBar.fillAmount = currentHealth / maxHealth;
    }

    public void TakeDamage(float damage,Vector2 attackDir){
        //damage(100-defense)/100 - takes defense into calculations(max def is 100)
		currentHealth -= (damage*(100-defense)/ 100);
        audio.Hurt();
        HealthBar.fillAmount = currentHealth / maxHealth;
        rigidbody.velocity = Vector2.zero;
        rigidbody.AddForce(new Vector2(attackDir.x, 0.5f) * 6, ForceMode2D.Impulse);
        if (currentHealth <=0){
			Dead();
            
        }
	}
	void Dead(){
		runMovementSpeed = 0;
		duckMovementSpeed = 0;
	}
	//should think of better wallcollision thing, tho it works great :D
	bool wallCollision(float dir){
		dir = dir>0 ? 1:-1;
		lookDirection =	(int) dir;
		Vector2 boxSize = GetComponent<BoxCollider2D>().size;
		for(int i = 0;i<3;i++){
			float yOffsetter = 0;
			if(i >0) yOffsetter = boxSize.y/i;
			Vector2 offSet = new Vector2(transform.position.x+dir*boxSize.x-dir*0.25f,transform.position.y+yOffsetter);
			RaycastHit2D hitted = Physics2D.Raycast(offSet,new Vector2(dir,0),0.5f);
			Debug.DrawRay(offSet,new Vector2(dir,0),Color.red,1);
			//Cases when to move and when not to move
			if(Vector2.Distance(hitted.point,offSet) == 0){
				if(hitted.collider != null)
					if(hitted.collider.GetComponent<Wearable>() != null || hitted.transform.tag == "Trigger")
						return false;
				return true;
			}
		}
		return false;
	}
	Vector3 changeDir(float dir, Transform transform){
		return new Vector3(dir, transform.localScale.y,transform.localScale.z);
	}
    public void isGrounded(bool val) {
        grounded = val;
        if(val)
            rigidbody.velocity = new Vector2(0,0);
        anim.SetBool("Airborne", !val);
        if (inventory.GetWeapon() != null)
            inventory.GetWeapon().GetAnimator().SetBool("Airborne", !val);
    }

    public void DecreaseMana(float ammount)
    {
        currentMana -= ammount;
        ManaBar.fillAmount = currentMana / maxMana;
    }
	public float getDamage(){
		return damage + inventory.GetWeapon ().damage;
	}

    void CheckUpgrades()
    {
        if (skillManager.upgrades.isUpdated)
        {
            maxHealth = 100 + skillManager.upgrades.Get(SimplePassiveType.HealthIncrease);
            runMovementSpeed = 5f + skillManager.upgrades.Get(SimplePassiveType.SpeedIncrease);
            jumpingForce = 10 + skillManager.upgrades.Get(SimplePassiveType.JumpSpeedIncrease);
            print(jumpingForce);
            inventory.GetWeapon().damage = Mathf.RoundToInt((50f + skillManager.upgrades.Get(SimplePassiveType.DamageIncrease) * skillManager.upgrades.Get(SimplePassiveType.DamageAmplification)));
        }
    }
}