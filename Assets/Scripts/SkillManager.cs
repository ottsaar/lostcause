﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour {

    //currently selected skills by the player
    SkillTreeNode[] selectedSkills;

    //skills that have been casted and have duration remaining
    List<Skill> activeSkills;

    //cooldowns for every skill that has been used
    IDictionary<string, float> cooldowns;


    private SkillTreeNode skillTree;
    private SkillFactory skillFactory;

    public int skillPointsAmmount = 0;

    private PlayerController player;

    [HideInInspector]
    public UpgradesDict upgrades;

    private void OnEnable()
    {
        skillTree = BuildSkillTree.Build();
        skillFactory = new SkillFactory();
        upgrades = new UpgradesDict();
    }

    void Start() {
        player = GetComponent<PlayerController>();

        cooldowns = new Dictionary<string, float>();
        activeSkills = new List<Skill>();
        selectedSkills = new SkillTreeNode[3];
    }

    // Update is called once per frame
    void Update () {
        updateActiveSkills();
	}
    private void LateUpdate()
    {
        upgrades.isUpdated = false;
    }

    /*
     * Calls update on active skill and reduces cooldowns of every skill in cooldowns dict
    */
    void updateActiveSkills()
    {
        int i = 0;
        foreach (Skill skill in activeSkills)
        {
            if (skill == null)
                continue;

            skill.Update();

            i++;
        }

        activeSkills.RemoveAll((Skill s) => { return s.IsFinished() == true; });

        List<string> keys = new List<string>(cooldowns.Keys);

        foreach (string key in keys)
        {
            cooldowns[key] = Mathf.Max(cooldowns[key] - Time.deltaTime, 0);
        }
    }

    /** 
    Casts a skill, adds it to cooldowns and active skills 
    index - skill index in selectedSkills

    */
    public void Cast(int index, Vector2 mousePosition)
    {

        foreach (Skill s in activeSkills)
        {
            if (s.TakesExtraInput() && s.skillData.skillName == selectedSkills[index].skill.skillName)
            {
                s.ExtraInput();
                return;
            }
        }

        if (CanCast(index))
        {
            Skill skill = skillFactory.MakeSkill(gameObject, mousePosition, (ActiveSkillData) selectedSkills[index].skill);

            if (skill.Activate())
            {
                player.DecreaseMana(skill.skillData.manaCost);
                activeSkills.Add(skill);
                cooldowns[selectedSkills[index].skill.skillName] = skill.getCooldown();
            }
        }
         
    }

    bool CanCast(int index)
    {
        if (selectedSkills[index] == null)
            return false;

        if (cooldowns.ContainsKey(selectedSkills[index].skill.skillName) && cooldowns[selectedSkills[index].skill.skillName] > 0f)
        {
            return false;  
        }

        if (((ActiveSkillData)selectedSkills[index].skill).manaCost >= player.currentMana)
        {
            return false;
        } 

        return true;
    }

    public SkillTreeNode GetSkillTree()
    {
        return skillTree;
    }

    public void AddSkillPoint()
    {
        skillPointsAmmount++;
    }


    public bool ActivateSkill(SkillTreeNode node)
    {
        if (skillPointsAmmount < node.skill.skillPointsCost)
            return false;

        if (node.Activate())
        {
            skillPointsAmmount--;
            upgrades.Update(skillTree);
            return true;
        }

        return false;
    }

    public void SelectSkill(int index, SkillTreeNode node)
    {
        selectedSkills[index] = node;
    }

    public SkillTreeNode GetSelectedSkill(int index)
    {
        return selectedSkills[index];
    }

    public float GetCooldownOfSkill(string name){
        if (cooldowns.ContainsKey(name))
            return cooldowns[name];
        else return 0f;
    }
}
