﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAudioManager : MonoBehaviour {
    public AudioClip walk;
    public AudioClip walk2;//optional
    public AudioClip hurt;
    public AudioClip jump;
    public AudioClip slash;
    public AudioClip dash;
    public AudioClip rope;
    public AudioClip drain;
    private AudioSource mov;
    private AudioSource act;
	// Use this for initialization
	void Start () {
        mov = GetComponents<AudioSource>()[0];
        act= GetComponents<AudioSource>()[1];
	}

    public void Walking()
    {
        if (!mov.isPlaying)
        {
            if(walk2 != null && Random.Range(0,2) == 1)
            {
                mov.clip = walk2;
            }
            else
            {
                mov.clip = walk;
            }
            mov.Play();
        }

    }
    public void Attack()
    {
        act.clip = slash;
        act.Play();
    }
    public void Dash()
    {
        mov.clip = dash;
        mov.Play();
    }
    public void Jump()
    {
        mov.clip = jump;
        mov.Play();
    }
    public void Hurt()
    {
        act.clip = hurt;
        act.Play();
    }
    public void Drain()
    {
        act.clip = drain;
        act.Play();
    }
    public void Rope()
    {
        act.clip = rope;
        act.Play();
    }
    public void setMovPlayBackSpeed(float speed)
    {
        mov.pitch = speed;
    }
    public void setActPlayBackSpeed(float speed)
    {
        act.pitch = speed;
    }
}
