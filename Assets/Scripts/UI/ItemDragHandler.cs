﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler,IEndDragHandler {
    public RectTransform itemPanel;
    public RectTransform mainPanel;
    private bool itemFromEquip = false;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (RectTransformUtility.RectangleContainsScreenPoint(itemPanel, Input.mousePosition))
        {
            itemFromEquip = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!RectTransformUtility.RectangleContainsScreenPoint(mainPanel, Input.mousePosition))
        {
            Debug.Log("Item Dropped");
            FindObjectOfType<Inventory>().DropItem(transform.parent.GetComponentInChildren<Wearable>().gameObject);
        }
        else if (RectTransformUtility.RectangleContainsScreenPoint(itemPanel, Input.mousePosition))
        {
            Debug.Log("Item Equipped");
            FindObjectOfType<Inventory>().Equip(transform.parent.GetComponentInChildren<Wearable>().gameObject);
        }
        else if (!RectTransformUtility.RectangleContainsScreenPoint(itemPanel, Input.mousePosition) && itemFromEquip)
        {
            Debug.Log("Item Deequipped");
            FindObjectOfType<Inventory>().Deequip(transform.parent.GetComponentInChildren<Wearable>().gameObject);
            itemFromEquip = false;
        }
        transform.localPosition = Vector3.zero;
        itemFromEquip = false;
    }
}
