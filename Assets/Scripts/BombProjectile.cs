﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombProjectile : MonoBehaviour {


    // Use this for initialization
    public Vector2 direction;
    public float speed;
    public Rigidbody2D rb;

    public float maxDamage;
    public float maxRange;

    public ParticleSystem particles;

    private bool ready;
    private bool ending;
   
	void Start () {
        direction.Normalize();
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0.3f;
        ready = false;
    }

    // Update is called once per frame
    void Update () {
        if(ready)
            rb.gravityScale += Time.deltaTime * 3;

        if (ending && !particles.isPlaying)
            Destroy(gameObject);
    }

    public void Launch()
    {
        rb.AddForce(direction * speed * 100 * Time.deltaTime, ForceMode2D.Impulse);
        ready = true;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Collider2D[] colarr = Physics2D.OverlapCircleAll(gameObject.transform.position, maxRange);

        particles.Play();
        Vector2 dir;
        float distance;

        foreach(Collider2D col in colarr)
        {
            EnemyController e = col.gameObject.GetComponent<EnemyController>();
            if (e != null) {
                dir = e.gameObject.transform.position - transform.position;
                distance = Mathf.Max((maxRange - dir.magnitude) / maxRange, 0.01f);
                e.gameObject.GetComponent<Rigidbody2D>().AddForce(dir * distance * 15 + 2 * Vector2.up, ForceMode2D.Impulse);
                e.TakeDamage(maxDamage * distance,transform.forward);

                print(dir * distance * 2 + 2 * Vector2.up);
             }

        }

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        ending = true;
        
    }



}
