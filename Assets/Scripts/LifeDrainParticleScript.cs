﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeDrainParticleScript : MonoBehaviour {


    private ParticleSystem ps;
    private ParticleSystem.Particle[] particles;
    public GameObject target;

	void Start () {
        ps = gameObject.GetComponent<ParticleSystem>();
        ps.transform.localScale = Vector3.one;
        particles = new ParticleSystem.Particle[200];

        ps.Play();
    }

    void LateUpdate()
    {
        int total = ps.GetParticles(particles);
        int i = 0;
        while (i < total)
        {
            Vector3 direction = target.transform.position + Vector3.up - particles[i].position;
            direction.Normalize();

            particles[i].position += direction * 5/particles[i].remainingLifetime * Time.deltaTime;
            if (Vector3.Distance(target.transform.position + Vector3.up, particles[i].position) < 0.3f)
                particles[i].remainingLifetime = -0.1f; 

            i++;
        }
        ps.SetParticles(particles, total);
    }

}
