﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillIconController : MonoBehaviour {


    public int skillKey;
    private SkillManager player;

	void Start () {
         player = GameObject.Find("Player").GetComponent<SkillManager>();
	}
	
	void Update () {
        SkillTreeNode node = player.GetSelectedSkill(skillKey);
        if (node == null)
        {
            GetComponentInChildren<Text>().text = "";
            return;
        }

        float cd = player.GetCooldownOfSkill(node.skill.skillName);
        if (cd == 0)
            GetComponentInChildren<Text>().text ="";
        else
            GetComponentInChildren<Text>().text = cd.ToString("0.0");
	}
}
