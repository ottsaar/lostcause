﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SkillTreeController : MonoBehaviour
{


    public Button NodePrefab;
    public Transform Content;
    public SkillManager player;
    public VerticalLayoutGroup statsPanel;

    public Text skillPointsPanelText;


    private SkillTreeNode tree;

    public float Separation;

    private bool active = false;
    private int index = 0;

    private Transform sView;
    // Use this for initialization

    void Start()
    {
        sView = transform.Find("Scroll View");
        sView.gameObject.SetActive(false);
        statsPanel.gameObject.SetActive(false);
        skillPointsPanelText.gameObject.transform.parent.gameObject.SetActive(false);
        tree = player.GetSkillTree();
        Button b = Instantiate<Button>(NodePrefab);
        b.transform.SetParent(Content);
        b.transform.position = GetComponentInChildren<RectTransform>().anchoredPosition;

        b.GetComponentInChildren<Text>().text = "0";
        tree.button = b;

        AddButtonData(b, tree);

        AddUINodeButtons(tree, 1, 0f, Mathf.PI * 2);
        index = 0;


        AddStatsTexts();
    }

    private void AddUINodeButtons(SkillTreeNode tree, int depth, float startAngle, float endAngle)
    {
        int childCount = tree.GetChildrenCount();
        float angle = startAngle;
        float angleForNode;

        foreach (SkillTreeNode child in tree.children)
        {
            angleForNode = ((endAngle - startAngle) / childCount);

            Button b = Instantiate<Button>(NodePrefab);
            AddButtonData(b, child);

            child.index = index;
            index++;

            b.transform.SetParent(Content);
            child.button = b;
            b.transform.position = GetComponentInChildren<RectTransform>().anchoredPosition + (new Vector2(Mathf.Cos(angle) * depth * Separation, Mathf.Sin(angle) * depth * Separation));
            b.GetComponentInChildren<Text>().text = depth.ToString();

            addLineToParent(child);


            AddUINodeButtons(child, depth + 1, angle - angleForNode / 2, angle + angleForNode / 2);

            angle += angleForNode;
        }

    }

    private void addLineToParent(SkillTreeNode node)
    {
        Vector2 nodePos = node.button.gameObject.transform.position;
        Vector2 parentPos = node.GetParent().button.gameObject.transform.position;
        Vector2 direction = parentPos - nodePos;
        direction.Normalize();

        node.button.GetComponentInChildren<UILine>().Points[1] = (parentPos - nodePos) - direction * 25;
        node.button.GetComponentInChildren<UILine>().Points[0] = direction * 25;


        if (node.IsActivated())
            node.button.GetComponentInChildren<UILine>().color = new Color(30f/255, 130f/255, 76f/255);
        else
            node.button.GetComponentInChildren<UILine>().color = new Color(0.5f, 0.5f, 0.5f); 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            active = !active;
            sView.gameObject.SetActive(active);
            statsPanel.gameObject.SetActive(active);
            skillPointsPanelText.gameObject.transform.parent.gameObject.SetActive(active);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && active)
        {
            active = !active;
            sView.gameObject.SetActive(active);
            statsPanel.gameObject.SetActive(active);
            skillPointsPanelText.gameObject.transform.parent.gameObject.SetActive(active);
        }


    }

    private void AddButtonData(Button b, SkillTreeNode node)
    {
        NodeButtonPanelController panel = b.GetComponentInChildren<NodeButtonPanelController>();
        if (node.skill == null) {
            panel.isActive = false;
            panel.gameObject.SetActive(false);
            return;
        }

        b.onClick.AddListener(() => OnNodeButtonClickHandler(b, node));
        panel.activateButton.onClick.AddListener(() => OnActivateButtonClickHandler(b, node));

        panel.skillName.text = node.skill.skillName;
        panel.description.text = node.skill.description;



        if (!node.skill.IsPassive())
        {
            panel.selectSkill1.onClick.AddListener(() => OnSelectButtonClickHandler(0, node));
            panel.selectSkill2.onClick.AddListener(() => OnSelectButtonClickHandler(1, node));
            panel.selectSkill3.onClick.AddListener(() => OnSelectButtonClickHandler(2, node));

        }
        else
        {
            panel.transform.Find("Panel").gameObject.SetActive(false);
        }

        panel.isActive = false;
        panel.gameObject.SetActive(false);

    }

    private void LateUpdate()
    {
        skillPointsPanelText.text = "Skillpoints: " + player.skillPointsAmmount;
    }

    public void OnNodeButtonClickHandler(Button b, SkillTreeNode node)
    {
        print("testing");
        HideAllOtherPanels(tree, b);
        b.GetComponentsInChildren<NodeButtonPanelController>(true)[0].Toggle();
    }

    private void HideAllOtherPanels(SkillTreeNode tree, Button b)
    {
        foreach (SkillTreeNode child in tree.children)
        {
            if (b == child.button)
            {
                HideAllOtherPanels(child, b);
                continue;
            }

            if (child.button.GetComponentInChildren<NodeButtonPanelController>() != null)
                child.button.GetComponentInChildren<NodeButtonPanelController>().Toggle(false);

            HideAllOtherPanels(child,b);
        }
    }

    public void OnActivateButtonClickHandler(Button b, SkillTreeNode node)
    {
        if (player.ActivateSkill(node))
        {
            UpdateUI(tree);
            b.GetComponentInChildren<NodeButtonPanelController>().activateButton.gameObject.SetActive(false);
        }
    }

    public void OnSelectButtonClickHandler(int index, SkillTreeNode node)
    {
        if(node.IsActivated())
            player.SelectSkill(index, node);
    }

    public void UpdateUI(SkillTreeNode tree)
    {
        UpdateUITree(tree);
        UpdateStatsTexts();
    }

    public void UpdateUITree(SkillTreeNode tree)
    {
        foreach (SkillTreeNode child in tree.children)
        {
            Button b = child.button;
            addLineToParent(child);
            UpdateUITree(child);
        }
    }

    private void AddStatsTexts()
    {
        Text original = statsPanel.GetComponentsInChildren<Text>(true)[0];

        foreach (KeyValuePair<SimplePassiveType, float> kv in player.upgrades.dict)
        {
            Text b = Instantiate<Text>(original);
            b.transform.SetParent(statsPanel.gameObject.transform);
            b.text = kv.Key.ToString() + ": " + kv.Value.ToString();
            b.gameObject.SetActive(true);
        }
    }

    private void UpdateStatsTexts() {
        Text[] texts = statsPanel.GetComponentsInChildren<Text>();
        int count = 0;

        foreach (KeyValuePair<SimplePassiveType, float> kv in player.upgrades.dict)
        {
            Text b = texts[count];
            b.text = kv.Key.ToString() + ": " + kv.Value.ToString();
            count++;
        }


    }

}
