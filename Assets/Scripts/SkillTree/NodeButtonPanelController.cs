﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeButtonPanelController : MonoBehaviour {

    public Button selectSkill1;
    public Button selectSkill2;
    public Button selectSkill3;

    public Button activateButton;
    public Text skillName;
    public Text description;

    [HideInInspector]
    public bool isActive = false;

    public SkillTreeNode node;
    // Use this for initialization

    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    public void Toggle()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void Toggle(bool a)
    {
        isActive = a;
        gameObject.SetActive(isActive);
    }


}
