﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillTreeNode
{

    private SkillTreeNode parent;
    private int level = 0;
    private bool activated = false;

    public int index;


    public SkillData skill = null;
    //TODO: button doesnt belong
    public Button button = null;
    public List<SkillTreeNode> children;


    public SkillTreeNode()
    {
        children = new List<SkillTreeNode>();
        parent = null;
    }

    public void AddChild(SkillTreeNode node)
    {
        children.Add(node);
        node.parent = this;
    }

    public int GetChildrenCount()
    {
        return children.Count;
    }

    public bool Activate()
    {
        if (parent == null)
        {
            activated = true;
            return true;
        }

        if (parent.IsActivated())
        {
            activated = true;
            return true;
        }
        else
            return false;
    }

    public bool IsActivated()
    {
        return activated;
    }

    public bool IsLeaf()
    {
        return children.Count == 0;
    }

    public SkillTreeNode GetParent()
    {
        return parent;
    }

}

