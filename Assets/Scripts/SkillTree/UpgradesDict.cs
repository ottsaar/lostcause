﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class UpgradesDict 
{
    public IDictionary<SimplePassiveType, float> dict;
    public bool isUpdated = false;

    public UpgradesDict()
    {
        dict = new Dictionary<SimplePassiveType, float>();
        InitValues();
    }

    public float Get(SimplePassiveType type)
    {
        return dict[type];
    }


    public void InitValues()
    {
        foreach (SimplePassiveType type in Enum.GetValues(typeof(SimplePassiveType)))
        {
            dict[type] =  0f;
        }
    }

    public bool Update(SkillTreeNode tree)
    {
        InitValues();
        isUpdated = true;
        return RecursUpdate(tree, false);

    }

    public bool RecursUpdate(SkillTreeNode tree, bool someValueChanged)
    {

        foreach (SkillTreeNode child in tree.children)
        {

            if (child.skill == null)
                continue;

            if (!child.IsActivated())
                continue;

            if (child.skill.IsPassive())
            {
                PassiveSkillData skill = (PassiveSkillData)child.skill;
                dict[skill.passiveType] += skill.param1;
                someValueChanged = true;
            }

            RecursUpdate(child, someValueChanged);
        }

        return someValueChanged;
    }
}
