﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 *  Information and parameters of the skill
*/

public abstract class SkillData : ScriptableObject {

    public string skillName;
    public string description;
    public Sprite skillImage;
    public int skillPointsCost;


    //parameters that are given to this type of skill's script
    //or if simple passive skills, then just param1 used to determine how much +dmg or w/e
  

    public abstract bool IsPassive();
}

