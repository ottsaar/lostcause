﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Skills/PassiveSkill")]
public class PassiveSkillData : SkillData
{
    public float param1;

    public SimplePassiveType passiveType;

    public override bool IsPassive()
    {
        return true;
    }
}
