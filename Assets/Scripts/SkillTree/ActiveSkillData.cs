﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Skills/ActiveSkill")]
public class ActiveSkillData : SkillData 
{
    public SkillType skillType;
    public float cooldown;
    public float manaCost;
    public float param1;
    public float param2;
    public float param3;

    public override bool IsPassive()
    {
        return false;
    }
}
