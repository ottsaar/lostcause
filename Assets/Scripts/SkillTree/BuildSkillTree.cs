﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSkillTree
{

    //script to build skilltree for testing ppurposes
    public static SkillTreeNode Build()
    {

        SkillTreeNode tree = new SkillTreeNode();
        tree.Activate();

        SkillTreeNode miniBomb = new SkillTreeNode();
        miniBomb.skill = Resources.Load<SkillData>("Active/MiniBomb");
        
        SkillTreeNode lifeDrain = new SkillTreeNode();
        lifeDrain.skill = Resources.Load<SkillData>("Active/LifeDrain");

        SkillTreeNode ropeThrow = new SkillTreeNode();
        ropeThrow.skill = Resources.Load<SkillData>("Active/RopeThrow");

        SkillTreeNode betterRopeThrow = new SkillTreeNode();
        betterRopeThrow.skill = Resources.Load<SkillData>("Active/BetterRopeThrow");



        SkillTreeNode dmgIncrease = new SkillTreeNode();
        dmgIncrease.skill = Resources.Load<SkillData>("Passive/DamageIncrease");

        SkillTreeNode dash = new SkillTreeNode();
        dash.skill = Resources.Load<SkillData>("Active/Dash");

        SkillTreeNode dash2 = new SkillTreeNode();
        dash2.skill = Resources.Load<SkillData>("Active/BetterDash");

        SkillTreeNode pullEverything = new SkillTreeNode();
        pullEverything.skill = Resources.Load<SkillData>("Active/PullEverything");

        SkillTreeNode pushEverything = new SkillTreeNode();
        pushEverything.skill = Resources.Load<SkillData>("Active/PushEverythingRadially");

        SkillTreeNode healthIncrease = new SkillTreeNode();
        healthIncrease.skill = Resources.Load<SkillData>("Passive/IncreaseHealth");

        SkillTreeNode dmgIncrease2 = new SkillTreeNode();
        dmgIncrease2.skill = Resources.Load<SkillData>("Passive/DamageIncrease2");

        SkillTreeNode dmgIncrease3= new SkillTreeNode();
        dmgIncrease3.skill = Resources.Load<SkillData>("Passive/DamageIncrease2");

        SkillTreeNode dmgIncrease4 = new SkillTreeNode();
        dmgIncrease4.skill = Resources.Load<SkillData>("Passive/DamageIncrease2");

        SkillTreeNode dmgAmp = new SkillTreeNode();
        dmgAmp.skill = Resources.Load<SkillData>("Passive/DamageAmplification");

        SkillTreeNode dmgAmp2 = new SkillTreeNode();
        dmgAmp2.skill = Resources.Load<SkillData>("Passive/DamageAmplification2");

        SkillTreeNode dmgAmp3 = new SkillTreeNode();
        dmgAmp3.skill = Resources.Load<SkillData>("Passive/DamageAmplification2");

        SkillTreeNode dmgAmp4 = new SkillTreeNode();
        dmgAmp4.skill = Resources.Load<SkillData>("Passive/DamageAmplification2");



        SkillTreeNode higherJump = new SkillTreeNode();
        higherJump.skill = Resources.Load<SkillData>("Passive/JumpIncrease");

        SkillTreeNode speedIncrease = new SkillTreeNode();
        speedIncrease.skill = Resources.Load<SkillData>("Passive/SpeedIncrease");

        SkillTreeNode speedIncrease2 = new SkillTreeNode();
        speedIncrease2.skill = Resources.Load<SkillData>("Passive/SpeedIncrease");

        SkillTreeNode speedIncrease3 = new SkillTreeNode();
        speedIncrease3.skill = Resources.Load<SkillData>("Passive/SpeedIncrease");

        SkillTreeNode speedIncrease4 = new SkillTreeNode();
        speedIncrease4.skill = Resources.Load<SkillData>("Passive/SpeedIncrease");



        tree.AddChild(lifeDrain);
            lifeDrain.AddChild(dash);
                dash.AddChild(dmgAmp3);
                    dmgAmp3.AddChild(dmgAmp4);
                dash.AddChild(dash2);

        tree.AddChild(ropeThrow);
            ropeThrow.AddChild(betterRopeThrow);
                betterRopeThrow.AddChild(speedIncrease);
            ropeThrow.AddChild(higherJump);
                higherJump.AddChild(speedIncrease2);
                    speedIncrease2.AddChild(speedIncrease3);
                    speedIncrease2.AddChild(speedIncrease4);

        tree.AddChild(dmgIncrease);
            dmgIncrease.AddChild(dmgIncrease2);
                dmgIncrease2.AddChild(dmgIncrease3);
                    dmgIncrease3.AddChild(dmgIncrease4);
            dmgIncrease.AddChild(dmgAmp);
                dmgAmp.AddChild(dmgAmp2);
            dmgIncrease.AddChild(miniBomb);


        tree.AddChild(healthIncrease);
            healthIncrease.AddChild(pullEverything);
            healthIncrease.AddChild(pushEverything);




        return tree;
    }
}
