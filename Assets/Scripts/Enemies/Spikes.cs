﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour {
	public float spikeDamage = 10;
	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Player"){
			col.GetComponent<EnemyController>().TakeDamage(spikeDamage,transform.forward);
		}
	}
}
