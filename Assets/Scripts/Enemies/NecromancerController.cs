﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NecromancerController : EnemyController {
	public List<Vector3> spots = new List<Vector3>();
	//place where to teleport, if special attack;
	public Vector3 safeSpot;
	//how long does special attack last
	public List<GameObject> minions = new List<GameObject>();
	public GameObject minion;
	//how fast is the new random generated, keep in mind the range in random changes according to health.
	private float actionChangeSpeed = 1f;
	private int currentSpot = 0;
	private float randomTimer;
	private bool doSpecial = false;
	private int specialLimit = 75;
	private int defaultSpawn = 2;
	private int toSpawn;
	private float defaultSpeed;
	private Weapon defaultWep;
	//Deployed on minion spawn
	public ParticleSystem worldParticle;

	void Start()
	{
		base.Start();
		defaultSpeed = base.enemySpeed;
		randomTimer = Time.time;
		defaultWep = base.enemyWep;
	}
	public override void Attack()
	{
		int randomAction = -1;
		if (currentHealth / maxHealth * 100 < specialLimit+target.GetComponent<PlayerController>().damage && !doSpecial) {
			transform.position = patrolPos1;
			transform.GetComponentInChildren<ParticleSystem> ().Play ();
		}
		if (currentHealth / maxHealth * 100 < specialLimit && !doSpecial) {
			toSpawn = defaultSpawn;
			doSpecial = true;
			specialLimit -= 25;
			base.enemySpeed = 0;
			base.enemyWep = null;
		}
		if(Time.time > randomTimer + actionChangeSpeed)
		{
			int rangeForRandom = (int)((currentHealth / maxHealth) * 7.5f + 1); //according to amount of health, how big of a change to do action
			randomAction = Random.Range(0, rangeForRandom);
			randomTimer = Time.time;
		}
		if (doSpecial) 
		{
			for (int i = 0; i < minions.Count; i++) {
				if (minions [i] == null) 
					minions.RemoveAt (i);
			}
			if (toSpawn == defaultSpawn) {
					transform.position = safeSpot;
					SpawnMinions ();
				}
			if (minions.Count == 0) {
				doSpecial = false;
				defaultSpawn += 2;
				transform.position = spots [0];
				base.enemySpeed = defaultSpeed;
				base.enemyWep = defaultWep;
			}
		}
		else if (randomAction == 0)
		{
			Teleport();
			randomTimer = Time.time;
		}
		else
		{
			base.Attack();
		}
	}
	//teleports boss to a new position
	void Teleport()
	{
		while (true)
		{ 
			int randomPlace = Random.Range(0, spots.Count);
			if (currentSpot != randomPlace)
			{
				//if player is too close then teleport else dont
				if (Vector3.Distance(target.position, transform.position) <= 3)
				{
					transform.GetComponentInChildren<ParticleSystem> ().Play ();
					currentSpot = randomPlace;
					transform.position = spots[currentSpot];
				}
				break;
			}
		}
	}
	void SpawnMinions()
	{
		worldParticle.Play ();
		while(toSpawn != 0){
			Vector3 spawnTo = spots[0];
			int sign = -5;
			if (toSpawn % 2 == 0) {
				sign = 5;
				spawnTo = spots [1];
			}
			GameObject made = Instantiate (minion, (spawnTo+new Vector3(toSpawn*sign,0,0)), Quaternion.identity);
			minions.Add(made);
			toSpawn -= 1;
		}
	}
}
