﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Controller : EnemyController {
	//spots to be teleported
    public List<Vector3> spots = new List<Vector3>();
	//how long does special attack last
	public float specialTime=2;
	//how fast is the new random generated, keep in mind the range in random changes according to health.
    public float actionChangeSpeed = 1f;
    private int currentSpot = 0;
    private float originalSpeed;
	private float shotTimer;
    private float randomTimer;

    void Start()
    {
        base.Start();
        originalSpeed = attackDelay;
		randomTimer = Time.time;
    }
    public override void Attack()
    {
        int randomAction = -1;
       
		if(Time.time > randomTimer + actionChangeSpeed)
        {
			int rangeForRandom = (int)((currentHealth / maxHealth) * 7.5f + 1); //according to amount of health, how big of a change to do action
			randomAction = Random.Range(0, rangeForRandom);
			randomTimer = Time.time;
        }
        if (randomAction == 0)
        {
            Teleport();
			randomTimer = Time.time;
        }
		else if(randomAction == 1 && shotTimer + specialTime < Time.time)
        {
            //faster attack..
            attackDelay = attackDelay * 0.25f;
			randomTimer = Time.time;
			shotTimer = Time.time;
        }
        else
        {
			if (Time.time > shotTimer + specialTime)
				base.attackDelay = originalSpeed;
            base.Attack();
        }
    }
    //teleports boss to a new position
    void Teleport()
    {
        while (true)
        { 
            int randomPlace = Random.Range(0, spots.Count);
            if (currentSpot != randomPlace)
            {
                //if player is too close then teleport else dont
                if (Vector3.Distance(target.position, transform.position) <= 3)
                {
					transform.GetComponentInChildren<ParticleSystem> ().Play ();
                    currentSpot = randomPlace;
                    transform.position = spots[currentSpot];
                }
                break;
            }
        }
    }
}
