﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float maxHealth = 3;
    public float attackDelay;
    public int attackDamage;
    public float attackingDistance = 0.65f;
    public float enemySpeed = 3;
    public float patrolSpeed = 1.5f;
    public float jumpStr = 4;
    public float currentHealth;
    public bool isPatrolling;
    public Weapon enemyWep;
    public Transform target;
    protected int lookDir = 1;
    private Rigidbody2D rigidbody;
    //patrolling positions
    public Vector2 patrolPos1;
    public Vector2 patrolPos2;
	public bool randomPatrolPos = true;
    private Vector2 patrolWaypoint;
    private bool patrolDir = true;//true - right, false - left
    private float timeCounter;
    protected CharAudioManager audio; 
	protected Animator anim;

    public void Start()
    {
        enemyWep.damage = attackDamage;
		enemyWep.boxCollider (false);
        rigidbody = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
		//if user wanted to make the patrol points random
		if (randomPatrolPos) {
			patrolPos1 = new Vector2(Random.Range(5, 10) + transform.position.x, 0);
			patrolPos2 = new Vector2(transform.position.x - Random.Range(5, 10), 0);
		}
        patrolWaypoint = patrolPos1;
        audio = GetComponent<CharAudioManager>();
        lookDir = (int)transform.localScale.x;
		anim = GetComponentsInChildren<Animator> () [0];
    }
    void Update()
    {
        Movement();
        RaySender();
		//for not taking unnessesary space
		if (transform.position.y < -500)
			Destroy (gameObject);
    }

    public void TakeDamage(float dmg,Vector2 attackDir)
    {
        if (target == null)
            target = FindObjectOfType<PlayerController>().transform;
        currentHealth -= dmg;
        audio.Hurt();
        rigidbody.velocity = Vector2.zero;
		if(attackDir != Vector2.zero)
        	rigidbody.AddForce(new Vector2(attackDir.x, 0.5f) *6, ForceMode2D.Impulse);
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
            GameObject.Find("Player").GetComponent<SkillManager>().AddSkillPoint(); ;
        }
    }

    void Movement()
    {
        if (target != null)
        {
            //If close to enemy attacking then attack
            if (Mathf.Abs(Vector3.Distance(transform.position, target.position)) <= attackingDistance)
            {
				anim.SetBool ("Moving", false);
                Attack();
                lookAt(target.position);
                return;
            }
            //Only x axis because the enemy shouldn't move upwards with out jumping
            transform.position = Vector3.MoveTowards(new Vector2(transform.position.x, transform.position.y), new Vector2(target.position.x, transform.position.y), enemySpeed * Time.deltaTime);
			anim.SetBool ("Moving", true);
			lookAt(target.position);
			audio.Walking();
            
        }
        else if (isPatrolling)
        {
            //If no target, then start moving around..
            PatrollingMovement(patrolSpeed);
			audio.Walking();
        }
        
    }

    void PatrollingMovement(float patrolSpeed)
    {
        if (transform.position.x >= patrolPos1.x && patrolDir)
        {
            patrolWaypoint = patrolPos2;
            patrolDir = false;
        }
        else if (transform.position.x <= patrolPos2.x && !patrolDir)
        {
            patrolWaypoint = patrolPos1;
            patrolDir = true;
        }
        transform.position = Vector3.MoveTowards(new Vector2(transform.position.x, transform.position.y), new Vector2(patrolWaypoint.x, transform.position.y), patrolSpeed * Time.deltaTime);
		anim.SetBool ("Moving", true);
		lookAt(patrolWaypoint);
    }

    public virtual void Attack()
    {
        if(Time.time > timeCounter + 0.5f)
        {
            enemyWep.boxCollider(true);
        }
        if (Time.time > timeCounter + attackDelay)
        {
            enemyWep.PerformAttack(1);
			anim.SetTrigger ("Attack");
            audio.Attack();
            timeCounter = Time.time;
        }

    }

    void lookAt(Vector2 waypoint)
    {
        if (transform.position.x > waypoint.x)
        {
            lookDir = -1;
        }
        else
        {
            lookDir = 1;
        }
        transform.localScale = new Vector2(lookDir, 1);
    }

    void RaySender()
    {
        for(int i = -1; i < 2; i++) { 
            RaycastHit2D hitted;
            Vector2 dir = new Vector2(1, i);
            float startRay = GetComponent<BoxCollider2D>().size.x / 2 + 0.01f;
            hitted = Physics2D.Raycast(transform.position + new Vector3(lookDir * startRay, 0, 0), lookDir * dir, 15);
            Debug.DrawRay(transform.position + new Vector3(lookDir * startRay, 0, 0), lookDir * dir, Color.red,2);
            if (hitted.collider == null)
                continue;
            if (Mathf.Abs(Vector3.Distance(transform.position, hitted.point)) < 0.65f)
            {
                if (hitted.transform.tag == "Player")
                {
                    target = hitted.transform;
                    break;
                }
                if (hitted.transform.tag == "NPCjumpable")
                {
                    rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0);//removes other velocity factors for adding jumpstr
                    rigidbody.AddForce(new Vector2(0, jumpStr), ForceMode2D.Impulse);
                }
                else
                {
                    if (hitted.transform.tag == "Weapon")
						break;
                    target = null;// So the enemy wouldn't follow forever(loses target)
                                  //if walks against wall while patrolling, turn around..
                    if (patrolWaypoint == patrolPos1)
                    {
                        patrolWaypoint = patrolPos2;
                        patrolDir = false;
                        break;
                    }
                    else
                    {
                        patrolWaypoint = patrolPos1;
                        patrolDir = true;
                        break;
                    }
                }
            }
            if (hitted.transform.tag == "Player")
            {
                target = hitted.transform;
                break;
            }
        }
    }


}
