﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RusherController : EnemyController {
	public override void Attack()
	{
		target = base.enemyWep.transform;
		transform.position = Vector3.MoveTowards(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x+base.lookDir, transform.position.y), enemySpeed * Time.deltaTime);
	} 
}
