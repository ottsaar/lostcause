﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterBossController : EnemyController {
	public GameObject shockWave;
	//how long does special attack last
	public float berserkerTime=2;
	//how fast is the new random generated, keep in mind the range in random changes according to health.
	public float actionChangeSpeed = 1f;
	private int currentSpot = 0;
	private float specialLimit = 75;
	private float originalAttackSpeed;
	private float originalMovSpeed;
	private float shotTimer;
	private float randomTimer;
	private float wavePower = 2;
	private float standTimer;
	private bool doSpecial = false;

	void Start()
	{
		base.Start();
		originalAttackSpeed = base.attackDelay;
		originalMovSpeed = base.enemySpeed;
		randomTimer = Time.time;
	}
	public override void Attack()
	{
		int randomAction = -1;

		//BERSERKER MODE TRIGGER!!!
		if (currentHealth / maxHealth * 100 < specialLimit) {
			specialLimit -= 25;
			//Double the speeds..
			if(base.enemySpeed == originalMovSpeed){
				base.enemySpeed = originalMovSpeed*2f;
				base.attackDelay = originalAttackSpeed * 0.5f;
			}
			shotTimer = Time.time;
		}
		if(Time.time > randomTimer + actionChangeSpeed)
		{
			int rangeForRandom = (int)((currentHealth / maxHealth) * 7.5f + 1); //according to amount of health, how big of a change to do action
			randomAction = Random.Range(0, rangeForRandom);
			randomTimer = Time.time;
		}
		//stands still for 0.5 sec, than special attack
		if (standTimer + 0.5f < Time.time && doSpecial) {
			SpecialAttack ();
			base.enemySpeed = originalMovSpeed;
			doSpecial = false;
			//not really a dash
			audio.Dash ();
		}
		if (randomAction == 0)
		{
			//instert cue animantion here
			standTimer = Time.time;
			base.enemySpeed = 0;
			doSpecial = true;
			anim.SetTrigger ("SpecialAttack");
		}
		else
		{	
			if (Time.time > shotTimer + berserkerTime && !doSpecial)
			{
				base.attackDelay = originalAttackSpeed;
				base.enemySpeed = originalMovSpeed;
				wavePower+=1;
			}
			if (Mathf.Abs (Vector3.Distance (transform.position, target.position)) <= 1.25f) {
				base.Attack ();
			} else {
				transform.position = Vector3.MoveTowards(new Vector2(transform.position.x, transform.position.y), new Vector2(target.position.x, transform.position.y), enemySpeed * Time.deltaTime);
				anim.SetBool ("Moving", true);
			}
				
		}
	}
	void SpecialAttack()
	{
		GameObject made = Instantiate (shockWave, transform.position+new Vector3(1.5f*lookDir,-0.25f,0), Quaternion.identity);
		made.GetComponent<ShockWave> ().SetPower (wavePower,lookDir);
	}
}
