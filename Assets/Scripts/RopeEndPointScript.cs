﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeEndPointScript : MonoBehaviour
{

    // Use this for initialization
    private bool moving;
    private Vector2 direction;
    private float speed;


    [HideInInspector]
    public bool done;
    [HideInInspector]
    public bool gotTarget;
    [HideInInspector]
    public float length;
    private float maxLength;



    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            Vector2 moveStep = direction * Time.deltaTime * speed;
            transform.position = new Vector2(transform.position.x, transform.position.y) + moveStep;
            length += moveStep.magnitude;

            if (length > maxLength)
            {
                moving = false;
                gotTarget = false;
                done = true;
            }
        }
    }

    public void StartMoving(Vector2 direction, float speed, float maxLength, Vector2 position)
    {
        this.direction = direction;
        transform.position = position;
        moving = true;
        this.speed = speed;
        this.maxLength = maxLength;
        length = 0f;
        done = false;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (length < 2 || done)
            return;

        done = true;
        gotTarget = true;
        moving = false;
    }
}
